<?php

namespace App\Http\Controllers\Gift;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Gift;
use App\GiftDetail;
use App\RedeemGift;
use App\User;
use PDO;

class GiftController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Gift Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles all process related to the gifts.
    |
    */
     
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    public function list_gift(Request $request)
    {
        $page = ($request->get('page') ? $request->get('page') : '1');
        $limit = ($request->get('limit') ? $request->get('limit') : '10');

        $gifts = Gift::orderBy('created_at', 'DESC')->orderBy('rating', 'DESC')->get();

        foreach ($gifts as $gift) {
            $gift_detail = GiftDetail::where('gift_id', $gift->id)->first();
            
            $n = $gift->rating;

            $resp[] = [
                "id" => $gift->id,
                "name" => $gift->name,
                "price_redeem" => $gift->price_redeem,
                "rating" => round($n),
                "image_url" => $gift_detail['image_url']

            ];

        }

        $respRaw = collect($resp);
        $response = $respRaw->forPage($page,$limit)->values();
        if(!$response){
            return false;
        }

        if(sizeof($response->toArray()) < 1){
            $responses = [
                'success' => true,
                'message' => "Sukses mengambil data gift",
                'data' => []
            ];

            return response()->json($responses, 200);
        } else{
            $responses = [
                'success' => true,
                'message' => "Sukses mengambil data gift",
                'data' => $response
            ];

            return response()->json($responses, 200);
        }
    }

    public function detail_gift($id)
    {
        $gift = Gift::with('gift_details')->where('id', $id)->first();
        
        if(!$gift){
            return false;
        }

        if(sizeof($gift->toArray()) < 1){
            $response = [
                'success' => true,
                'message' => "Sukses mengambil data gift",
                'data' => []
            ];

            return response()->json($response, 200);
        } else{
            $response = [
                'success' => true,
                'message' => "Sukses mengambil data gift",
                'data' => $gift
            ];

            return response()->json($response, 200);
        }
    }

    public function create_gift(Request $request)
    {
        $rq = $request->all();

        $makeGift = new Gift();
        $makeGift->name = $rq["gift_name"];
        $makeGift->price_redeem = $rq["gift_price_redeem"];
        $makeGift->stock = $rq["gift_stock"];
        $makeGift->rating = 0;
        $makeGift->save();

        $makeDetailGift = new GiftDetail();
        $makeDetailGift->gift_id = $makeGift->id;
        $makeDetailGift->description = $rq["gift_description"];
        $makeDetailGift->image_url = $rq["gift_image_url"];

        if(!$makeDetailGift){
            return false;
        } else{
            $response = [
                'success' => true,
                'message' => "Sukses menyimpan data gift",
            ];

            return response()->json($response, 200);
        }
    }

    public function update_gift(Request $request, $id)
    {
        $rq = $request->all();

        $update_gift = Gift::where('id', $id)->update([
            "name" => $rq["gift_name"],
            "price_redeem" => $rq["gift_price_redeem"],
            "stock" => $rq["gift_stock"],
            "rating" => 0
        ]);

        $update_gift_detail = GiftDetail::where('gift_id', $id)->update([
            "description" => $rq["gift_description"],
            "image_url" => $rq["gift_image_url"],
        ]);

        if(!$update_gift_detail){
            return false;
        } else{
            $response = [
                'success' => true,
                'message' => "Sukses modifikasi data gift",
            ];

            return response()->json($response, 200);
        }
    }

    public function update_gift_attributes(Request $request, $id)
    {
        $rq = $request->all();

        $update_gift_attrib = Gift::where('id', $id)->update([
            "price_redeem" => $rq["gift_price_redeem"],
            "stock" => $rq["gift_stock"],
        ]);

        if(!$update_gift_attrib){
            return false;
        } else{
            $response = [
                'success' => true,
                'message' => "Sukses modifikasi attribute data gift",
            ];

            return response()->json($response, 200);
        }
    }

    public function delete_gift($id)
    {

        $delete_gift_detail = GiftDetail::where('gift_id', $id)->delete();
        $delete_gift = Gift::where('id', $id)->delete();

        if(!$delete_gift){
            return false;
        } else{
            $response = [
                'success' => true,
                'message' => "Sukses menghapus attribute data gift",
            ];

            return response()->json($response, 200);
        }
    }

    public function redeem_gift(Request $request)
    {
        $rq = $request->all();
        $token = str_replace("Bearer ", "",$request->header("Authorization"));

        $getuser_id = User::where("api_token", $token)->first();
        $redeem_id = "RD-".rand(0001, 1000);

        foreach ($rq["redeem"] as $gift) {
            $getStock = Gift::where('id', $gift['gift_id'])->first();

            if($getStock['stock'] < 1){
                $deleteRedeem = RedeemGift::where('redeem_id', $redeem_id)->delete();

                $response = [
                    'success' => true,
                    'message' => "Maaf gift ".$getStock['name']." yang dipilih kehabisan stock",
                ];
    
                return response()->json($response, 200);
            }
            $redeemGift = new RedeemGift();
            $redeemGift->redeem_id = $redeem_id;
            $redeemGift->gift_id = $gift['gift_id'];
            $redeemGift->gift_qty = $gift['qty'];
            $redeemGift->gift_rating = 0;
            $redeemGift->user_id = $getuser_id['id'];
            $redeemGift->save();
            
        }

        if(!$redeemGift){
            return false;

        } else{
            foreach ($rq["redeem"] as $gift) {

                $decStock = Gift::where('id', $gift['gift_id'])->decrement("stock", (int)$gift['qty']);
            }

            $response = [
                'success' => true,
                'message' => "Sukses mereedem data gift",
            ];

            return response()->json($response, 200);
        }
    }

    public function rating_gift(Request $request)
    {
        $rq = $request->all();
        $token = str_replace("Bearer ", "",$request->header("Authorization"));

        $getuser_id = User::where("api_token", $token)->first();

        $rating_gift = RedeemGift::where('user_id', $getuser_id['id'])->where('gift_id', $rq['gift_id'])->update([
            "gift_rating" => $rq['rating']
        ]);

        if(!$rating_gift){
            return false;

        } else{

            $response = [
                'success' => true,
                'message' => "Sukses rating gift!",
            ];

            return response()->json($response, 200);
        }
    }
}
