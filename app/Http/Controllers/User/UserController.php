<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\User;

class UserController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | User Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */
     
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    public function login_user(Request $request)
    {

        $model = User::where(['email' => $request['email']])->select(['name','email','password','remember_token', 'api_token'])->first();
        
        if(!$model)
            return false;

        if(Hash::check($request['password'],$model->password)) {
            $token = md5(uniqid());

            $updateToken = User::where(['email' => $request['email']])->update([
                "api_token" => $token
            ]);

            $response = [
                'success' => true,
                'message' => "Anda telah berhasil login",
                'access_token' => $token
            ];

            return response()->json($response, 200);            
            // Right password
        } else {
            // Wrong one
            $response = [
                'success' => true,
                'message' => "Maaf password yang anda masukan salah",
                'access_token' => ""
            ];

            return response()->json($response, 400);            
        }

        return false;
    }

    public function details_user(Request $request)
    {
        print_r($request->header("Authorization"));die;
    }
}
