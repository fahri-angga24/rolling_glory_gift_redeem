<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Gift extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'price_redeem', 'stock', ' rating'
    ];

    protected $table = "gifts";

    public function gift_details()
    {
    	return $this->hasMany('App\GiftDetail', 'gift_id', 'id');
    }

}
