<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class RedeemGift extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'redeem_id', 'gift_id', 'gift_qty', 'gift_rating', 'user_id'
    ];

    protected $table = "redeem_list";

}
