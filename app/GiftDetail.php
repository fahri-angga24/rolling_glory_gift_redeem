<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class GiftDetail extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'gift_id', 'description', 'image_url'
    ];

    protected $table = "gifts_detail";

    public function gift()
    {
    	return $this->belongTo('App\Gift');
    }

}
