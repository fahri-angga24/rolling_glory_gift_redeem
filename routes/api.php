<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/userss', function (Request $request) {
    return $request->user();
});


Route::post('users/login', 'User\UserController@login_user');
Route::middleware('auth:api')->group(function () {
    Route::post('users/register', 'User\UserController@register');
    Route::get('users', 'User\UserController@details_user');
    Route::put('users/{id}', 'User\UserController@update_user');
    Route::delete('users/{id}', 'User\UserController@delete_user');
    Route::get('gifts', 'Gift\GiftController@list_gift');
    Route::get('gifts/{id}', 'Gift\GiftController@detail_gift');
    Route::post('gifts', 'Gift\GiftController@create_gift');
    Route::put('gifts/{id}', 'Gift\GiftController@update_gift');
    Route::patch('gifts/{id}', 'Gift\GiftController@update_gift_attributes');
    Route::delete('gifts/{id}', 'Gift\GiftController@delete_gift');
    Route::post('gifts/id/redeem', 'Gift\GiftController@redeem_gift');
    Route::post('gifts/id/rating', 'Gift\GiftController@rating_gift');
});

